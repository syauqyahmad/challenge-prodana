<?php

use App\Http\Controllers\SiswaController;
use App\Http\Controllers\KelasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','App\Http\Controllers\SiswaController@index');
Route::post('/daftar','App\Http\Controllers\SiswaController@store');
Route::delete('/{siswa}/delete', 'App\Http\Controllers\SiswaController@destroy')->name('siswa.destroy');
Route::patch('/{siswa}/edit', 'App\Http\Controllers\SiswaController@update')->name('siswa.update');
Route::get('//form/{id}','App\Http\Controllers\SiswaController@edit')->name('siswa.edit');  
Route::get('/kelas','App\Http\Controllers\KelasController@index');  
Route::post('/masuk','App\Http\Controllers\KelasController@store');  
Route::delete('/kelas/{kelas}/delete','App\Http\Controllers\KelasController@destroy')->name('kelas.destroy');  
Route::patch('/kelas/{kelas}/edit','App\Http\Controllers\KelasController@update')->name('kelas.update');
Route::get('/kelas/form/{id}','App\Http\Controllers\KelasController@edit')->name('kelas.edit'); 
