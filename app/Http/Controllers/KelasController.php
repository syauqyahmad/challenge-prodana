<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kelas;

class KelasController extends Controller
{
    public function index(){
        $data = kelas::latest()->paginate(5);
        return view ('kelas', compact('data'));
    }

    public function create()  
   {  
     //  
   }  

   public function store(Request $request){
    $data = new kelas();  
    $data->kode_kelas = $request->kode_kelas;  
    $data->nama_kelas = $request ->nama_kelas;  
    $data->save();  
    return redirect('/kelas')->with('toast_success', 'Data Berhasil Tersimpan'); 
   }

   public function show($id)  
   {  
     //  
   }  

   public function edit($id)  
   {  
     $kelas = kelas::findorfail($id);  
     return view('formkelas',compact('kelas'));  
   }  

   public function update(kelas $kelas)  
   {  
     $kelas->update([  
       'kode_kelas' => request('kode_kelas'),  
       'nama_kelas' => request('nama_kelas')  
     ]);  
     return redirect('/kelas');  
   }

   public function destroy(kelas $kelas)  
   {  
    $kelas->delete();  
    return redirect('/kelas');  
   }  
}
