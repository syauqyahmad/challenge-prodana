<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\siswa;
use App\Models\kelas;

class SiswaController extends Controller
{
    public function index(){
        $data = siswa::with('get_kelas')->latest()->paginate(7);  
        $datas = kelas::all();  
        return view('siswa',compact('data', 'datas'));   
    }

    public function create()  
    {  
      //
    } 

    public function store(Request $request)  
   {  
     $data = new siswa(); 
     $data->kode_siswa = $request->kode_siswa;  
     $data->nama_siswa = $request ->nama_siswa;  
     $data->email = $request->email;  
     $data->no_hp = $request->no_hp;
     $data->id_kelas = $request->id_kelas;    
     $data->save(); 

     return redirect('/');  
   }
   
   public function show($id)  
   {  
       //
   }
   
   public function edit($id)  
   {  
     $siswa = siswa::with('get_kelas')->findorfail($id);  
     $datas = kelas::All();  
     return view('formsiswa',compact('siswa','datas'));
   }
   
   public function update(siswa $siswa)  
   {  
     $siswa->update([  
        'kode_siswa' => request('kode_siswa'),  
        'nama_siswa' => request('nama_siswa'),  
        'email' => request('email'),  
        'no_hp' => request('no_hp'),  
        'id_kelas' => request('id_kelas')
      ]);  
     return redirect('/');  
   }  

   public function destroy(siswa $siswa)
   {
    $siswa->delete();
    return redirect('/');
   }
}
