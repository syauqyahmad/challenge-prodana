<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    protected $primarykey = 'id';
    
    protected $fillable = ['id', 'kode_siswa', 'nama_siswa', 'email', 'no_hp', 'id_kelas'];

    protected $table = 'siswa';

    public function get_kelas(){
        return $this->belongsTo('App\Models\kelas', 'id_kelas');
    }
}
