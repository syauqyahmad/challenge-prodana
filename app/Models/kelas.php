<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kelas extends Model
{
    protected $table = 'kelas';
    
    protected $primarykey = 'id';

    protected $fillable = ['id', 'kode_kelas', 'nama_kelas'];

    public function get_siswa(){  
        return $this->hasMany('App\Models\siswa', 'id_kelas');  
    } 
}
