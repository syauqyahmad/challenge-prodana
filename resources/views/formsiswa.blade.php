<!DOCTYPE html>  
 <html lang="en">  
 <head>  
 <meta charset="utf-8">  
 <meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">  
 <title>Daftar Kelas</title>  
 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">  
 <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">  
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
 <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">  
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>  
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
 </head>  
 <body>  
   <div class="container">  
     <div class="table-wrapper">  
       <div class="table-title">  
         <div class="row">  
           <div class="col-sm-6">  
            <h2>Form <b>Kelas</b></h2>  
           </div>  
         </div>  
       </div>  
      <form method="POST" action="{{ route('siswa.update',$siswa) }}">    {{ csrf_field() }}  
        {{ method_field('PATCH') }}  
      <div class="modal-body">  
       <div class="form-group">  
        <label>Kode Siswa</label>  
        <input type="text" class="form-control" name="kode_siswa" value="{{$siswa->kode_siswa}}">  
       </div>  
       <div class="form-group">  
        <label>Nama Siswa</label>  
        <input type="text" class="form-control" name="nama_siswa" value="{{$siswa->nama_siswa}}">  
       </div>       
       <div class="form-group">  
        <label>Email</label>  
        <input type="email" class="form-control" name="email" value="{{$siswa->email}}">  
       </div>       
       <div class="form-group">  
        <label>No HP</label>  
        <input type="text" class="form-control" name="no_hp" value="{{$siswa->no_hp}}">  
       </div>
       <div class="form-group">  
        <label>Nama Kelas</label>  
        <select class="form-control" name="id_kelas"> 
            <option disabled value>Pilih Kelas</option> 
          @foreach($datas as $kelas)  
            <option value="{{$kelas->id}}">{{$kelas->nama_kelas}}</option>  
          @endforeach  
         </select>  
       </div>       
      </div>  
      </div>  
      <div class="modal-footer">  
        <a href="/" class="btn btn-default"> Cancel </a>   
       <input type="submit" class="btn btn-info" value="Save">  
     </div>  
    </form>  
   </div>  
  </div>