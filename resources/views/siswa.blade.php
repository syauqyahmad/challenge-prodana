<!DOCTYPE html>  
<html lang="en">  
<head>  
<meta charset="utf-8">  
<meta http-equiv="X-UA-Compatible" content="IE=edge">  
<meta name="viewport" content="width=device-width, initial-scale=1">  
<title>Daftar Siswa</title>  
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">  
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
</head>  
<body>  
  <div class="container">  
    <div class="table-wrapper">  
      <div class="table-title">  
        <div class="row">  
          <div class="col-sm-6">  
      <h2>Data <b>Siswa & Kelas</b></h2>  
     </div>  
     <div class="col-sm-6">  
      <a href="#addStudentModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>CREATE</span></a>  
     </div>  
        </div>  
      </div>  
      <table class="table table-striped table-hover">  
        <thead>  
          <tr>  
            <th>Kode</th>  
            <th>Nama</th>  
            <th>Email</th>  
            <th>No HP</th>  
            <th>Kelas</th>  
            <th>Actions</th>  
          </tr>  
        </thead>  
        <tbody>  
          @foreach($data as $siswa)  
          <tr>  
            <td>{{$siswa->kode_siswa}}</td>  
            <td>{{$siswa->nama_siswa}}</td>  
            <td>{{$siswa->email}}</td>  
            <td>{{$siswa->no_hp}}</td>  
            <td>{{$siswa->get_kelas->nama_kelas}}</td>    
            <td><form action="{{ route('siswa.edit',$siswa->id) }}" method="get"> 
              {{ csrf_field() }} 
             <button type="submit" class="btn btn-primary btn-sm" style="float:left;">Update</button>  
            </form>  
              <form action="{{ route('siswa.destroy',$siswa->id) }}" method="post">  
              {{ csrf_field() }}  
               <input type="hidden" name="_method" value="DELETE">  
               <button type="submit" class="btn btn-danger btn-sm" style="margin-left:3px;">Delete</button>  
              </form>  
            </td>  
          </tr>  
          @endforeach  
        </tbody>  
      </table>  
      <div class="text-center">
        {{ $data->links() }}
      </div>
      <div class="footer">  
       <table>  
        <tr>  
         <td> <form action="/">  
          <button type="submit" class="btn btn-primary btn-sm">Daftar Siswa</button>  
          </form></td>  
         <td><form action="/kelas">  
          <button type="submit" class="btn btn-primary btn-sm">Daftar Kelas</button>  
          </form></td>  
        </tr>  
       </table>  
      </div>  
    </div>  
  </div>  
 <!-- membuat pembeli -->  
 <div id="addStudentModal" class="modal fade">  
  <div class="modal-dialog">  
   <div class="modal-content">  
    <form method="POST" action="/daftar">  
     {{ csrf_field()}}  
     <div class="modal-header">        
      <h4 class="modal-title">CREATE</h4>  
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
     </div>  
     <div class="modal-body">       
      <div class="form-group">   
       <label>Kode</label>  
       <input type="text" class="form-control" name="kode_siswa">  
      </div>  
      <div class="form-group">  
       <label>Nama</label>  
       <input type="text" class="form-control" name="nama_siswa">  
      </div>  
      <div class="form-group">  
       <label>Email</label>  
       <input type="email" class="form-control" name="email">  
      </div>
      <div class="form-group">  
        <label>No. HP</label>  
        <input type="text" class="form-control" name="no_hp">  
       </div>   
      <div class="form-group">  
       <label>Kelas</label>  
       {{-- <input type="text" class="form-control" name="nama_kelas"> --}}
       <select class="form-control" name="id_kelas">
        <option disabled value>Pilih Kelas</option>  
        @foreach($datas as $kelas)  
        <option value="{{$kelas->id}}">{{$kelas->nama_kelas}}</option>  
        @endforeach  
       </select>  
      </div>   
      </div>       
     <div class="modal-footer">
       <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">  
       <input type="submit" class="btn btn-success" value="Add">  
     </div>  
    </form>  
   </div>  
  </div>  
 </div>  
</body>  
</html>