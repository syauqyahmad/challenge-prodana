<!DOCTYPE html>  
 <html lang="en">  
 <head>  
 <meta charset="utf-8">  
 <meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">  
 <title>Daftar Kelas</title>  
 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">  
 <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">  
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
 <link rel="stylesheet" type="text/css" href="{{asset('css/style.css') }}">  
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>  
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
 </head>  
 <body>  
   <div class="container">  
     <div class="table-wrapper">  
       <div class="table-title">  
         <div class="row">  
           <div class="col-sm-6">  
       <h2>Daftar <b>Kelas</b></h2>  
      </div>  
      <div class="col-sm-6">  
       <a href="#addClassModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>CREATE</span></a>  
      </div>  
         </div>  
       </div>  
       <table class="table table-striped table-hover">  
         <thead>  
           <tr>  
             <th>Kode Kelas</th>  
             <th>Nama Kelas</th>  
             <th>Created At</th>  
             <th>Update At</th>  
             <th>Actions</th>  
           </tr>  
         </thead>  
         <tbody>  
          @foreach($data as $kelas)  
           <tr>  
             <td>{{$kelas->kode_kelas}}</td>  
             <td>{{$kelas->nama_kelas}}</td>  
             <td>{{$kelas->created_at}}</td>  
             <td>{{$kelas->updated_at}}</td>  
             <td><form action="{{ route ('kelas.edit',$kelas->id) }}" method="get">  
               {{ csrf_field()}}  
              <button type="submit" class="btn btn-primary btn-sm" style="float:left;">Update</button>  
             </form>  
               <form action="{{ route('kelas.destroy',$kelas->id) }}" method="post">  
               {{ csrf_field() }}  
                <input type="hidden" name="_method" value="DELETE">  
                <button type="submit" class="btn btn-danger btn-sm" style="margin-left:3px;">Delete</button>  
               </form>  
             </td>  
           </tr>  
          @endforeach  
         </tbody>  
       </table>
       <div class="text-center">
         {{ $data->links() }}  
       </div>
       <div class="footer">  
        <table>  
         <tr>  
          <td> <form action="/">  
           <button type="submit" class="btn btn-primary btn-sm">Daftar Siswa</button>  
           </form></td>  
          <td><form action="/kelas">  
           <button type="submit" class="btn btn-primary btn-sm">Daftar Kelas</button>  
           </form></td>  
         </tr>  
        </table>  
       </div>  
     </div>  
   </div>  
  <!-- Membuat daftar barang -->  
  <div id="addClassModal" class="modal fade">  
   <div class="modal-dialog">  
    <div class="modal-content">  
     <form method="POST" action="/masuk">  
      {{ csrf_field() }}  
      <div class="modal-header">        
       <h4 class="modal-title">CREATE</h4>  
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
      </div>  
      <div class="modal-body">       
       <div class="form-group">   
        <label>Kode Kelas</label>  
        <input type="text" class="form-control" name="kode_kelas">  
       </div>  
       <div class="form-group">  
        <label>Nama Kelas</label>  
        <input type="text" class="form-control" name="nama_kelas">  
       </div>     
      </div>  
      <div class="modal-footer">  
        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">  
        <input type="submit" class="btn btn-success" value="Add">  
      </div>  
     </form>  
    </div>  
   </div>  
  </div>  
 </body>  
 </html>